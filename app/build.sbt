lazy val commonSettings = Seq(
  scalaVersion := "2.11.7",
  libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
  libraryDependencies += "io.argonaut" %% "argonaut" % "6.0.4",
  libraryDependencies += "io.reactivex" %% "rxscala" % "0.25.0"
)

lazy val db = (project in file("DB")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies += "org.mongodb.scala" % "mongo-scala-driver_2.11" % "1.1.0"
  )
lazy val crawler = (project in file("Crawler")).
  settings(commonSettings: _*).
  settings(
    mainClass in Compile := Some("com.trendy.crawler.Main")
  ).dependsOn(db)
