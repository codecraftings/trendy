import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Created by amish on 1/10/2016.
  */
object Boomerang {
  def main(args: Array[String]): Unit = {
    val T = Console.in.readLine().toInt
    for (t <- 0 until T) {
      val N = Console.in.readLine().toInt
      val points: ListBuffer[(Int, Int)] = new ListBuffer[(Int, Int)]
      for (i <- 0 until N) {
        val arr = Console.in.readLine().split(' ')
        val x = arr(0).toInt
        val y = arr(1).toInt
        //println(x, y)
        points.append((x, y))
      }
      val boomMap = mutable.Map.empty[String, Boolean]
      for ((x, y) <- points) {
        val distMap = mutable.Map.empty[Int, List[(Int, Int)]]
        for ((x2, y2) <- points; if x != x2 || y != y2; dist = (x - x2) * (x - x2) + (y - y2) * (y - y2)) {
          //println(x, y, x2, y2)
          if (distMap.contains(dist)) {
            //println("boom")
            boom(distMap(dist))
            distMap(dist) = (x2, y2) :: distMap(dist)
          } else {
            distMap(dist) = List((x2, y2))
          }
          def boom(l: List[(Int, Int)]): Unit = {
            //println("boom2")
            l foreach {
              case (x3, y3) => {
                val hash = getBoomHash(x, x2, x3, y, y2, y3)
                //println(hash)
                boomMap(hash) = true
              }
            }
          }
        }
      }
      println(boomMap.size)
    }

  }

  def getBoomHash(x1: Int, x2: Int, x3: Int, y1: Int, y2: Int, y3: Int): String = {
    def d(x: Int, y: Int): Int = (x + 10000) + (y + 10000)*20000
    val d1 = d(x1, y1);
    val d2 = d(x2, y2);
    val d3 = d(x3, y3)
    if (d1 >= d2) {
      if (d2 >= d3) d1 + "-" + d2 + "-" + d3 else if (d3 >= d1) d3 + "-" + d1 + "-" + d2 else d1 + "-" + d3 + "-" + d2
    }
    else if (d1 >= d3) d2 + "-" + d1 + "-" + d3 else if (d3 >= d2) d3 + "-" + d2 + "-" + d1 else d2 + "-" + d3 + "-" + d1
  }
}
