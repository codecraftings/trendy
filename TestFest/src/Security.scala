/**
  * Created by amish on 1/10/2016.
  */
object Security {
  def main(args: Array[String]): Unit = {
    val T = Console.in.readLine().toInt
    for (t <- 0 until T) {
      val N = Console.in.readLine().toInt
      val r1 = Console.in.readLine().toArray
      val r2 = Console.in.readLine().toArray
      var flag = false
      for (i <- 0 until N) {
        if (r1(i) == '.') {
          if (r2(i) == '.' && (i == (N - 1) || r2(i + 1) == 'X') && (i == 0 || r2(i - 1) == 'X')) {
            r1(i) = 'G'
            flag = false
            var j = i + 1
            while (j<N&&r1(j) != 'X') {
              r1(j) = '-'
              j += 1
            }
          } else flag = true
        } else if (r1(i) == 'X') {
          if (flag) {
            r1(i - 1) = 'G'
            flag = false
          }
        }
      }
      if (flag) {
        r1(N - 1) = 'G'
        flag = false
      }
      for (i <- 0 until N) {
        if (r2(i) == '.') {
          if (r1(i) == 'G' && (i == (N - 1) || r1(i + 1) == 'X') && (i == 0 || r1(i - 1) == 'X')) {
            r2(i) = 'G'
            r1(i) = '.'
            flag = false
            var j = i + 1
            while (j<N&&r2(j) != 'X') {
              r2(j) = '-'
              j += 1
            }
          } else if (r1(i) != 'G') flag = true
        } else if (r2(i) == 'X') {
          if (flag) {
            if (r1(i - 1) != 'G') r2(i - 1) = 'G'
            else if (i >= 2) r2(i - 2) = 'G'
            flag = false
          }
        }
      }
      if (flag) {
        r2(N - 1) = 'G'
        flag = false
      }
      val count = r1.foldRight(0)((c, i) => {
        if (c == 'G') i + 1 else i
      }) + r2.foldRight(0)((c, i) => if (c == 'G') i + 1 else i)
      //println(r1.mkString(" "), "||", r2.mkString(" "))
      println("Case #"+(t+1)+": "+count)
    }
  }
}
