import scala.collection.mutable.ListBuffer

var ll = System.nanoTime()
def l(): Unit = {
  println(System.nanoTime() - ll)
  ll = System.nanoTime()
}
def remove[A](n: Int, l: List[A]): (List[A], A) = {
  l.zipWithIndex.foldRight((List[A](), null.asInstanceOf[A]))((i, tup) => {
    if(i._2==n){
      (tup._1, i._1)
    }else{
      (i._1::tup._1, tup._2)
    }
  })
}
remove(0, List('a,'b,'c,'d,'e,'f))
remove(2, List(1))